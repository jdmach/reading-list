# Economic Reading List

## Completed

## In Progress

- *Basic Economics: A Common Sense Guide to the Economy* 5th Ed. by Thomas Sowell

## To-Do

- *Common Sense Economics: What Everyone Should Know About Wealth and Prosperity* by James D. Gwartney
- *The Millionaire Next Door: The Surprising Secrets of America's Wealthy* by Thomas J. Stanley