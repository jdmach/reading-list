# Fiction Reading List

## Completed

- *Tree and Leaf* by J.R.R. Tolkien - 7/30/2023

## In Progress


## To-Do

### Historical Fiction

- *Gettysburg: An Alternate History* by Peter G. Tsouras
- *Isaac: Trek to King's Mountain* - by J. Wayne Fears
