# Political Reading List

## Completed

- *The Theft of America’s Soul: Blowing the Lid Off the Lies That Are Destroying Our Country* by Phil Robertson
- *unPHILtered: The Way I See It* by Phil Robertson
- *Going Public: An Organizer's Guide to Citizen Action* by Michael Gecan

## In Progress

- *Socialism: The Real History from Plato to the Present, How the Deep State Capitalizes on Crisis to Consolidate Control* - by William J. Federer

## To-Do

- *Making Hate Pay: The Corruption of the Southern Poverty Law Center* by Tyler O'Neil
- *Takedown: From Communists to Progressives, How the Left Has Sabotages Family and Marriage* by Paul Kengor
- *The Right Side of History: How Reason and Moral Purpose Made the West Great* by Ben Shapiro
- *The Madness of Crowds: Gender, Race and Identity* by Douglas Murray
- *Apocalypse Never: Why Environmental Alarmism Hurts Us All* by Michael Shellenberger
- *Charter Schools and Their Enemies* by Thomas Sowell
- *American Marxism* by Mark R. Levin
