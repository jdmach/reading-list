# Interpersonal Reading List

## Completed

- *What Every Body Is Saying: An Ex-FBI Agent's Guide to Speed-Reading People* by Joe Navarro, Marvin Karlins - 10/2021

## In Progress

## To-Do

- *High Performance Habits: How Extraordinary People Become That Way* by Brendon Burchard
