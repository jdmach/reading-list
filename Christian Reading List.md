# Christian Reading List

## Completed

### Apologetics

- *God's Not Dead: Evidence for God in an Age of Uncertainty* by Rice Broocks, Thomas Nelson
- *Total Truth: Liberating Christianity from Its Cultural Captivity (Study Guide Edition)* by Nancy Pearcey
- *Saving Leonardo: A Call to Resist the Secular Assault on Mind, Morals, & Meaning* by Nancy Pearcey
- *Teaching Others to Defend Christianity: What Every Christian Should Know* by Cathryn S. Buse
- *Love Thy Body: Answering Hard Questions About Life and Sexuality* by Nancy Pearcey
- *God Behaving Badly: Is the God of the Old Testament Angry, Sexist and Racist?* by David T. Lamb
- *Awake & Alive To Truth - Finding Truth in the Chaos of a Relativistic World* by John L. Cooper
- *The New Evidence That Demands A Verdict* by Josh McDowell - 7/2022

### Theology

- *Slave: The Hidden Truth About Your Identity in Christ* by John MacArthur - 8/2022
- *The Thrill of Orthodoxy: Rediscovering the Adventure of Christian Faith* by Trevin Wax - 6/2023

### Personal Growth

- *Knowledge of the Holy* by A.W. Tozer
- *The Grand Weaver: How God Shapes Us Through the Events of Our Lives* by Ravi Zacharias
- *The Imperfect Disciple: Grace for People Who Can't Get Their Act Together* by Jared C. Wilson
- *Si-renity: How I Stay Calm and Keep the Faith* by Si Robertson
- *The Freedom of Self Forgetfulness: The Path to True Chrisitian Joy* by Timothy Keller
- *The Bondage Breaker* by Neil T. Anderson
- *The Gospel-Centered Life* by Robert H. Thune, Will Walker
- *Secrets of the Kingdom Life* by David Bercot
- *The Mortification of Sin - Unabridged* by John Owen
- *Marry Wisely, Marry Well: A Blueprint for Personal Preparation* by Ernie Baker
- *The Deeply Formed Life* by Rich Villodas
- *Every Man's Battle: Winning the War on Sexual Temptation One Victory at a Time* by Stephen Arterburn, Fred Stoeker, Mike Yorkey
- *The Dating Manifesto: A Drama-Free Plan for Pursuing Marriage with Purpose* by Lisa Anderson
- *Spirit Walk: The Extraordinary Power of Acts for Ordinary People* by Steve Smith 
- *Emotionally Healthy Spirituality: It's Impossible to be Spiritually Mature, While Remaining Emotionally Immature* by Peter Scazzero - 8/2021
- *The Meaning of Marriage: Facing the Complexities of Commitment with the Wisdom of God* by Timothy Keller with Kathy Keller - 7/2022, 11/2023
- *Death to Deconstruction: Reclaiming Faithfulness as an Act of Rebellion* by Joshua S. Porter - 12/2022
- *In the Name of Jesus: Reflections on Christain Leadership* by Henry J.M. Nouwen - 4/2024

### Evangelism

- *Power Evangelism* by John Wimber, Kevin Springer

### Special Topics

- *Of Games and God: A Christian Exploration of Video Games* by Kevin Schut
- *Images and Idols: Creativity for the Christian Life* by Thomas J. Terry, J. Ryan Lister, Jackie Hill-Perry

### Allegory/Fiction

- *The Divine Comedy* by Dante Alighieri
- *The Pilgrim's Progress* by John Bunyan
- *A story reserved for Such a Time as This* by Russ F. Carman

### Histories/Memoirs

- *Face to Face: Where Life Meets Reality* by Tarrance Weaver
- *Fighter: Five Keys to Conquering Fear and Reaching Your Dreams* by Christopher Greenwood aka Manafest, Shannon Constantine Logan
- *Tortured for Christ* by Richard Wurmbrand

### Devotionals

- *Through the Bible Through the Year* by John Stott
- *"Lord, Give Us the Nations!" The Foundational Teachings of the College of Prayer* - 8/2022
- *God's Wisdom for Navigating Life: A Year of Daily Devotions in the Book of Proverbs* by Timothy Keller with Kathy Keller - 3/2023

## In Progress

- *Beyond Opinion: Living the Faith We Believe* by Ravi Zacharias, et al.
- *The Complete Works of Francis A. Schaeffer - Volume 1: A Christian View of Philosophy and Culture* by Francis A. Schaeffer

## Upcoming

- *The Complete Works of Francis A. Schaeffer - Volume 2: A Christian View of The Bible as Truth* by Francis A. Schaeffer
- *The Complete Works of Francis A. Schaeffer - Volume 3: A Christian View of Spiritualisy* by Francis A. Schaeffer
- *The Complete Works of Francis A. Schaeffer - Volume 4: A Christian View of The Church* by Francis A. Schaeffer
- *The Complete Works of Francis A. Schaeffer - Volume 5: A Christian View of The West* by Francis A. Schaeffer

## To-Do

### Theology - To-Do

- *Commentary on Galatians* by Martin Luther

### Apologetics - To-Do

- *The Case for Life: Equipping Christians to Engage the Culture* by Scott Klusendorf
- *Is God Just a Human Invention? And Seventeen Other Questions Raised by the New Atheists* by Sean McDowell
- *God's Battalions: The Case for the Crusades* by Rodney Stark
- *The Moral Argument: A History* by David Baggett
- *The Case for the Resurrection of Jesus* by Gary R. Habermas
- *A Practical Guide to Culture* by Brett Kunkle, John Stonestreet
- *The End of Reason: A Response to the New Atheists* by Ravi Zacharias
- *God’s Crime Scene: A Cold-Case Detective Examines the Evidence for a Divinely Created Universe* by J. Warner Wallace
- *Transferrable Cross Training: Training Christian Soldiers for Spiritual Battle - 2 Apologetics* by Karl I. Payne
- *Atonement and the Death of Christ: An Exegetical, Historical, and Philosophical Exploration* by William Lane Craig
- *What Do They Believe - A Systematic Theology of the Major Western Religions* by Andrew Rappaport
- *Tactics: A Game Plan for Discussing Your Christian Convictions* by Greg Koukl
- *The Story of Reality: How the World Began, How It Ends, and Everything Important that Happens in Between* by Greg Koukl
- *Why Should I Believe Christainity? (The Big Ten)* by James N. Anderson
- *The Historical Jesus: Ancient Evidence for the Life of Christ* by Gary R. Habermas

### Personal Growth - To-Do

- *Mere Christianity* by C.S. Lewis, Kathleen Norris
- *God in the Dock* by C.S. Lewis
- *Go and Sin No More: A Call to Holiness* (Kindle Version) by Michael L. Brown
- *Every Good Endeavor: Connecting Your Work to God's Work* by Timothy Keller, Katherine Leary Alsdorf
- *Transferrable Cross Training: Training Christian Soldiers for Spiritual Battle - 1 Essentials* by Karl I. Payne
- *Transferrable Cross Training: Training Christian Soldiers for Spiritual Battle - 3 Leadership* by Karl I. Payne
- *The Dude's Guide to Manhood: Finding True Manliness in a World of Counterfeits* by Darrin Patrick
- *Driven by Eternity: Make Your Life Count Today & Forever* by John Bevere
- *Love Your God with All Your Mind: The Role of Reason in the Life of the Soul* by J.P. Moreland
- *Readings for Meditation and Reflection* by C.S. Lewis
- *Winning the War in Your Mind: Change Your Thinking, Change Your Life* by Craig Groeschel
  
### Histories/Memoirs - To-Do

- *Wurmbrand: Tortured for Christ - The Complete Story* by The Voice of the Martyrs
- *Our Hands are Stained with Blood: The Tragic Story of the Church and the Jewish People* by Michael L. Brown
- *I Can Only Imagine: A Memoir* by Bart Millard
- *Grace Behind Bars: An Unexpected Path to True Freedom* by Bo Mitchell
- *The Insanity of God: A True Story of Faith Resurrected* by Nik Ripken
- *Losing my Voice to Find It: How Rockstar Discovered His Greatest Purpose* by Mark Stuart
- *Confessions* by Saint Augustine

### Other - To-Do

- *Money, Greed, and God 10th Anniversary Edition: The Christian Case for Free Enterprise* by Jay W. Richards
- *Donald Trump is Not My Savior: An Evangelical Leader Speaks His Mind About the Man He Supports as President* by Michael L. Brown
- *Christianity and Liberalism, new ed.* by J. Gresham Machen
- *Talking with Your Kids about God* by Natasha Crain
- *The Prodigal Church: A Gentle Manifesto against the Status Quo* by Jared C. Wilson
- *Live Not by Lies: A Manual for Christian Dissidents* - Rod Dreher
