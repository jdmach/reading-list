# Historical Reading List

## Completed

### Second World War

- *Life and Death in Nazi Germany* by Peter Fritzsche
- *Little Man, What Now* by Hans Fallada
- *Neighbors* by Jan T. Gross
- *Citizen Soldiers: The U. S. Army from the Normandy Beaches to the Bulge to the Surrender of Germany* by Stephen E. Ambrose
- *Killing Patton: The Strange Death of World War II's Most Audacious General* by Bill O'Reilly, Martin Dugard
- *Soldier, Sailor, Frogman, Spy, Airman, Gangster, Kill or Die: How the Allies Won on D-Day* by Giles Milton - 10/2022

### American History

- *New Sprits: Americans in the “Gilded Age,” 1865-1905, Third Edition* by Rebecca Edwards
- *A Godly Hero, The Life of William Jennings Bryan* by Michael Kazin
- *A Fierce Discontent: The Rise and Fall of the Progressive Movement in America, 1870-1920* by Michael McGerr

### War on Terror

- *Lone Survivor: The Eyewitness Account of Operation Redwing and the Lost Heroes of Seal Team 10* by Marcus Luttrell with Patrick Robinson
- *Red Platoon: A True Story of American Valor* by Clinton Romesha - 12/2022

### Biographies

- *A Godly Hero: The Life of William Jennings Bryan* - by Michael Kazin - 2/2020

## In Progress


## To-Do

- *A Shopkeeper's Millennium: Society and Revivals in Rochester, New York, 1815-1837* by Paul E. Johnson
- *A Patriot's Memoirs of World War II: Through My Eyes, Heart, and Soul* by Luciano "Louis" Charles Graziano
- *Skunk Works: A Personal Memoir of My Years at Lockheed* by Ben R. Rich
- *Letters Written from the English Front in France Between September 1914 and March 1915* by Sir Edward Hamilton Westrow Hulse
- *The Gulag Archipelago - An Experiment In Literary Investigation* by Aleksandr Solzhenitsyn
- *Desert Storm Air War: The Aerial Campaign against Saddam's Iraq in the 1991 Gulf War* by Jim Corrigan
- *Beyond The Call: The True Story of One World War II Pilot's Covert Mission to Rescue POWs on the Eastern Front* by Lee Trimble
